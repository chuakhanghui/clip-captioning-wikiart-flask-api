import argparse
import base64
import os
import tempfile
from io import BytesIO

import cv2
import numpy as np
import skimage.io
from flask import Flask
from flask_cors import CORS
from flask_restx import Api, Resource
from PIL import Image, ImageFile
from werkzeug.datastructures import FileStorage

from model  import ClipCaptionModel, generate_beam, generate2
from transformers import GPT2Tokenizer
import numpy as np
import clip
import torch
import re
from collections import OrderedDict

WEIGHTS_PATHS = {
    "pretrained-coco": "/workspace/weights/coco_weights.pt",
    "pretrained-conceptual": "/workspace/weights/conceptual_weights.pt",
    "from-scratch": "/workspace/weights/scratch-27.ckpt",
    "from-coco": "/workspace/weights/coco-23.ckpt",
    "from-conceptual": "/workspace/weights/conceptual-19.ckpt",
    # "wikiart-clean_overfit": "/workspace/notebook/wikiart-captioning/weight_torch_wikiart/train-epoch=29.ckpt",
    # "transfer_coco_overfit": "/workspace/notebook/wikiart-captioning/weight_torch_coco/wikiart_eos-train-epoch=29.ckpt",
    # "transfer-conceptual-overfit": "/workspace/notebook/wikiart-captioning/weight_torch_conceptual/wikiart_eos-train-epoch=29.ckpt",
}
PREFIX_LENGTH = 10
DEVICE = torch.device("cpu")
CPU = torch.device("cpu")

ImageFile.LOAD_TRUNCATED_IMAGES = True
Image.MAX_IMAGE_PIXELS = None

def assert_correct_image_format(image):
    assert image.dtype == np.uint8

def base64_to_image(s):
    """Decode an image encoded as a base64 string to an image array."""

    if ";base64," in s:
        s = s.split(";base64,")[-1]

    img = np.asarray(Image.open(BytesIO(base64.b64decode(s))))
    return img


def load_state_dict(weight_path):
    torch_dict = torch.load(weight_path, map_location=CPU)
    if "pytorch-lightning_version" in torch_dict:
        state_dict = torch_dict["state_dict"]
        state_dict = OrderedDict({k.replace("net.",""): v for k,v in state_dict.items()})
    else:
        state_dict = torch_dict
    return state_dict

def load_model():
    # Load CLIP adn Caption Model
    clip_model, clip_preprocess = clip.load("ViT-B/32", device=DEVICE, jit=False)
    tokenizer = GPT2Tokenizer.from_pretrained("gpt2")

    models = {}
    for key, weights_path in WEIGHTS_PATHS.items():
        model = ClipCaptionModel(PREFIX_LENGTH)
        model.load_state_dict(load_state_dict(weights_path))
        model = model.eval()
        model = model.to(DEVICE)
        models[key] = model

    # model = ClipCaptionModel(PREFIX_LENGTH)
    # model = model.eval()
    # model = model.to(DEVICE)

    return models, clip_model, clip_preprocess, tokenizer


def get_flask_app():
    app = Flask(__name__)
    CORS(app)

    api = Api(app, version=0.1, title="wikiart-captioning")

    req_parser = api.parser()
    req_parser.add_argument("image",
                            location="files",
                            type=FileStorage,
                            required=False,
                            help="image file")
    req_parser.add_argument("images_b64",
                            type=str,
                            action="append",
                            required=False,
                            help="image b64 strings")
    req_parser.add_argument("image_urls",
                            type=str,
                            action="append",
                            required=False,
                            help="image URLs")
    req_parser.add_argument("beam_search",
                            type=bool,
                            required=True,
                            default=True,
                            help="Use Beam Search")

    @api.route('/predict')
    @api.expect(req_parser)
    class WikiArtCaptioning(Resource):
        def post(self):
            req_args = req_parser.parse_args()
            if (not req_args.image) and (not req_args.images_b64) and (not req_args.image_urls):
                return {"error": "Please provide one of 'image', 'images_b64', or 'image_urls'."}, 400

            images = []
            if req_args.image:
                suffix = os.path.splitext(req_args.image.filename)[-1]
                with tempfile.NamedTemporaryFile(suffix=suffix) as f:
                    req_args.image.save(f.name)
                    image = skimage.io.imread(f.name)
                    assert_correct_image_format(image)
                    images.append(image)
            elif req_args.images_b64:
                for s in req_args.images_b64:
                    image = base64_to_image(s)
                    assert_correct_image_format(image)
                    images.append(image)
            else:
                for url in req_args.image_urls:
                    image = skimage.io.imread(url)
                    assert_correct_image_format(image)
                    images.append(image)

            predicted_captions = predict(images, req_args.beam_search)
            return predicted_captions, 200

    return app

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=5000)
    args = parser.parse_args()

    print("Starting Server at port", args.port)
    print("Loading Models...")
    # Load model
    models, clip_model, clip_preprocess, tokenizer = load_model()

    # model.load_state_dict(torch.load(weights_path, map_location=CPU))
    print("Models loaded!")

    def predict(images, beam_search) -> dict:
        # x = preprocess_images(images)
        results = {}
        
        for i, img in enumerate(images):
            pil_image = Image.fromarray(img)
            clip_image = clip_preprocess(pil_image).unsqueeze(0).to(DEVICE)

            with torch.no_grad():
                captions = {}
                # for w_key, w_path in WEIGHTS_PATHS.items():
                for w_key in WEIGHTS_PATHS:
                    # Replace Weight
                    # model.load_state_dict(torch.load(w_path))
                    model = models[w_key]

                    prefix = clip_model.encode_image(clip_image).to(DEVICE, dtype=torch.float32)
                    prefix_embed = model.clip_project(prefix).reshape(1, PREFIX_LENGTH, -1)
                    end_token = "." if "pretrained" in w_key else None
                    if beam_search:
                        gen_caption = generate_beam(model, tokenizer, embed=prefix_embed, stop_token=end_token)[0]
                    else:
                        gen_caption = generate2(model, tokenizer, embed=prefix_embed, stop_token=end_token)
                    gen_caption = gen_caption.replace("<|endoftext|>", "")
                    gen_caption = re.sub(r"(.+?)\1+", r"\1", gen_caption)
                    
                    captions[w_key] = gen_caption

            results[i] = captions

        return results

    app = get_flask_app()
    app.run("0.0.0.0", port=args.port, debug=False)
