# Start 

## New Folder

> mkdir wikiart

> cd wikiart

## Clone This Repo

> git clone [repo]

## Get Weight

> aws s3 cp s3://ckhui-misc/weights/wikiart/ ./weights/ --recursive

## Docker Build

> cd wikiart-captioning-api/

> docker build .

# Run on Startup

## systemd

> sudo cp -v ./docker_boot.service /etc/systemd/system

> sudo systemctl enable docker_boot.service

> sudo systemctl start docker_boot.service

## check service status

> sudo systemctl status docker_boot.service

## reboot and check if api is up

> sudo reboot
