FROM pytorch/pytorch:1.9.0-cuda10.2-cudnn7-runtime

WORKDIR /workspace

RUN apt update && apt upgrade -y
RUN apt install -y git libgl1-mesa-glx libglib2.0-0

COPY requirements.txt .
RUN pip install -r requirements.txt